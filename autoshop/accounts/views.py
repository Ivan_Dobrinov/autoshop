from django.urls import reverse_lazy
from django.views.generic import CreateView
from . import models, forms


class UserSignUpView(CreateView):
    model = models.User
    form_class = forms.UserCreateForm
    template_name = 'accounts/login.html'
    success_url = reverse_lazy('index')
