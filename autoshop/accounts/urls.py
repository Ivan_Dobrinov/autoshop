from django.conf.urls import url
from django.urls import reverse_lazy
from django.contrib.auth import views as auth
from . import views

app_name = 'accounts'

urlpatterns = [
    url(r'^login/$',
        auth.LoginView.as_view(template_name='accounts/login.html', success_url=reverse_lazy('index')),
        name='login'),
    url(r'^logout/$', auth.LogoutView.as_view(template_name='accounts/logged_out.html'), name='logout'),
    url(r'^signup/$', views.UserSignUpView.as_view(), name='signup'),
]
