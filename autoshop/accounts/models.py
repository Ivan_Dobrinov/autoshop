from django.db import models as db_models
from django.contrib.auth import models


# Create your models here.

class User(models.User, models.PermissionsMixin):
    USER_TYPE_CHOICES = (
        ('pr', 'PR'),
        ('mech', 'Mechanic'),
        ('client', 'Client'),
    )

    user_type = db_models.CharField(max_length=6, choices=USER_TYPE_CHOICES, default='client')

    def __str__(self):
        return self.username
